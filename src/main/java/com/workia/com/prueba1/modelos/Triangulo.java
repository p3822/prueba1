/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workia.com.prueba1.modelos;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 *
 * @author yair
 */
public class Triangulo extends Figura{

    public Triangulo() {
        this.base = 2.0;
        this.altura = 3.0;
    }

    
    @Override
    public Double getSuperficie() {
        return (base * altura)/2;
    }

    @Override
    public Double getBase() {
        return base;
    }

    @Override
    public Double getAltura() {
        return altura;
    }

    @Override
    public Double getDiametro() {
        return null;
    }

    @Override
    public String getTipoFigura() {
        return tipo;
    }
    
}
