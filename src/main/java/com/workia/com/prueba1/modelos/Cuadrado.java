/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workia.com.prueba1.modelos;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 *
 * @author yair
 */
public class Cuadrado extends Figura{

    public Cuadrado() {
        this.base = 3.0;
    }

    
    @Override
    public Double getSuperficie() {
        return base* base;
    }

    @Override
    public Double getBase() {
        return base;
    }

    @Override
    public Double getAltura() {
        return null;
    }

    @Override
    public Double getDiametro() {
        return diametro;
    }

    @Override
    public String getTipoFigura() {
        return tipo;
    }
    
}
