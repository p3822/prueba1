/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workia.com.prueba1.modelos;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j;

/**
 *
 * @author yair
 */
public class Circulo extends Figura {

    public Circulo() {
        this.diametro = 3.0;
    }

    
    @Override
    public Double getSuperficie() {
        return Math.sqrt( Math.PI * Math.pow((diametro/2), 2));
    }

    @Override
    public Double getBase() {
        return null;
    }

    @Override
    public Double getAltura() {
        return null;
    }

    @Override
    public Double getDiametro() {
        return diametro;
    }

    @Override
    public String getTipoFigura() {
        return tipo;
    }
    
}
