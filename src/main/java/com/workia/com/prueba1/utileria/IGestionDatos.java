/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workia.com.prueba1.utileria;

/**
 *
 * @author yair
 */
public interface IGestionDatos {
    public Double getSuperficie();
    public Double getBase();
    public Double getAltura();
    public Double getDiametro();
    public String getTipoFigura();
    
}
