package com.workia.com.prueba1.utileria;

import com.workia.com.prueba1.modelos.Circulo;
import com.workia.com.prueba1.modelos.Cuadrado;
import com.workia.com.prueba1.modelos.Figura;
import com.workia.com.prueba1.modelos.Triangulo;


public class FiguraFactory {

	
	public static  Figura getFigura(String tipo){
		if(tipo.equals(Constantes.CIRCULO)) {
			return new Circulo();
		}
		if(tipo.equals(Constantes.CUADRADO)) {
			return new Cuadrado();
		}
		if(tipo.equals(Constantes.TRIANGULO)) {
			return new Triangulo();
		}
		return null;
	}
	
}
