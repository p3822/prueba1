/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workia.com.prueba1;

import com.workia.com.prueba1.modelos.Figura;
import com.workia.com.prueba1.utileria.Constantes;
import com.workia.com.prueba1.utileria.FiguraFactory;
import lombok.extern.java.Log;
/**
 *
 * @author yair
 */
@Log
public class main {
    public static void main(String[] args) {
        Figura figura = FiguraFactory.getFigura(Constantes.CIRCULO);
        log.info("----"+Constantes.CIRCULO+" ----");
        log.info("diametro: "+figura.getDiametro().toString());
        log.info(figura.getSuperficie().toString());
         
        figura = FiguraFactory.getFigura(Constantes.CUADRADO);
        log.info("----"+Constantes.CUADRADO+" ----");
        log.info("base: "+figura.getBase().toString());
        log.info("superficie: "+figura.getSuperficie().toString());
        
        figura = FiguraFactory.getFigura(Constantes.TRIANGULO);
        log.info("----"+Constantes.TRIANGULO+" ----");
        log.info("base: "+figura.getBase().toString());
        log.info("altura: "+figura.getAltura().toString());
        log.info("superficie: "+figura.getSuperficie().toString());
    }
   
}
